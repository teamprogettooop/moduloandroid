package com.smartdoor.moduloandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.lang.ref.WeakReference;

public class InDoorActivity extends AppCompatActivity {
    TextView logView;

    private static InDoorActivityHandler uiHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_door);
        logView = findViewById(R.id.log_text);
        uiHandler = new InDoorActivityHandler(this);
        Log.i("ModuloAnd Handle InDoor", "Creata InDoorActivity.");

        Button btn_close = findViewById(R.id.close_btn);
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Door.getInstance().close();
            }
        });

        Button btn_intensity = findViewById(R.id.intensity_button);
        btn_intensity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView intensity = findViewById(R.id.intensity_text);
                if(intensity.getText().toString().isEmpty()) {
                    Log.w("ModuloAnd Handle InDoor", "Valore intensita' vuoto");
                } else {
                    int intensity_v = Integer.valueOf(intensity.getText().toString());
                    Log.w("ModuloAnd Handle InDoor", "Valore intensita' " + intensity.getText().toString());
                    if(intensity_v < 0 || intensity_v > 100) {
                        logView.setText(R.string.intensity_value_err);
                    } else {
                        if(Door.getInstance().setIntensity(intensity_v)) {
                            // Ho cambiato l'intensità.
                            logView.setText(R.string.change_intensity_ok);
                            TextView intensity_label = findViewById(R.id.intensity_label);
                            intensity_label.setText(getString(R.string.intensity_label_text) + ": " + Door.getInstance().getIntensity());
                            intensity.setText("0");
                        } else {
                            // Non ho cambiato l'intensità.
                            logView.setText(R.string.change_intensity_no);
                        }
                    }
                }
            }
        });
    }

    public void onStart() {
        super.onStart();
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public static InDoorActivityHandler getHandler() {
        return uiHandler;
    }

    public static class InDoorActivityHandler extends Handler {
        private WeakReference<InDoorActivity> context;

        private InDoorActivityHandler(InDoorActivity context) {
            this.context = new WeakReference<>(context);
        }

        public void handleMessage(Message msg) {
            Object obj = msg.obj;
            Log.i("ModuloAnd Handle InDoor", "Sono in handle.");
            if(obj instanceof String) {
                String message = obj.toString();
                switch(message) {
                    //TODO: Quando non si fa vedere dal pir o se clicca il bottone arduino o se clicca sul bottone android.
                    case "exit":
                        Log.w("ModuloAnd Handle InDoor", "Sono dentro a exit.");
                        context.get().setResult(RESULT_OK);
                        context.get().finish();
                        break;
                    //TODO: Quando arriva il messaggio di intensita' o di temperatura
                    default:
                        Log.w("ModuloAnd Handle InDoor", "Sono dentro a temp.");
                        // TODO:Switchare tra temperatura e intensita'
                        TextView tlabel = context.get().findViewById(R.id.temperature_label);
                        tlabel.setText(context.get().getString(R.string.temperature_label_text) + message);
                        break;
                }
            }
        }

    }
}

