package com.smartdoor.moduloandroid;

public enum DoorState {
    DOOR_OPEN,
    DOOR_CLOSE
}
